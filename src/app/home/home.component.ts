import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { TopicService } from '../services/topic.service';

import { Topic } from '../topic';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.styles.css'],
    providers: [TopicService]
})

export class HomeComponent implements OnInit {
    constructor(private route: ActivatedRoute,
                private router: Router,
                private topicService: TopicService) { }

    topics: Topic[];

    ngOnInit(): void {
        this.topicService.getTopics().then(topics => {
            this.topics = topics
        });
    }

    onSelect(topicIndex: number): void {
        this.router.navigate(['./topic', topicIndex]);
    }
}
