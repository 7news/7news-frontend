import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { TopicService } from '../services/topic.service';
import { Topic } from '../topic';
import { Article } from '../topic';

/*
const TOPICS: Topic[] = [
    { id: 1, title: "asdfaf" },
    { id: 2, title: "sdfgdggggg" },
];
*/

@Component({
    selector: 'article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.styles.css'],
    providers: [TopicService]
})

export class ArticleComponent implements OnInit {
    constructor(private route: ActivatedRoute,
                private router: Router,
                private topicService: TopicService) { }

    //topics = TOPICS;
    topic: Topic;
    topicId: number;
    articleId: number;
    article: Article;

    ngOnInit(): void {
        this.topicId = this.route.snapshot.params['topicId'];
        this.articleId = this.route.snapshot.params['articleId'];

        this.topicService.getTopics().then(topics => {
            this.topic = topics[this.topicId];
            this.article = this.topic.articles[this.articleId];
        });
    }

    onSelect(articleIndex: number): void {
        alert(articleIndex);
    }
}
