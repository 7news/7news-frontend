import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
    backButtonActive: Boolean;

    constructor(private _location: Location,
                private router: Router,
                private route: ActivatedRoute) {
        router.events.subscribe((val) => {
            if (val.url.indexOf('/topic') >= 0) {
                this.backButtonActive = true;
            }
            else {
                this.backButtonActive = false;
            }
        });
    }

    ngOnInit() {
        console.log('Application Initialized!');
    }

    backClicked() {
        this._location.back();
    }
}
