import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { TopicDetailComponent } from './topicDetail/topicDetail.component';
import { ArticleComponent } from './article/article.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'topic/:id', component: TopicDetailComponent},
    { path: 'topic/:topicId/article/:articleId', component: ArticleComponent},
    { path: 'about', component: AboutComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})

export class AppRoutingModule {}
