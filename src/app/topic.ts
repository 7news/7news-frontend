export class Article {
    id: string;
    title: string;
    paragraphs: string[];
    dateCreated: Date;
}

export class Topic {
    description: string;
    articles: Article[];
}

/*
{
"description": "Manbij District: Azaz District, Syrian Civil War, Syrian Democratic Forces",
"articles": [
    {
    "id": "tag:reuters.com,2017:newsml_KCN1BR0Q5:1",
    "title": "Russia knew U.S.-backed Syrian forces were in area it bombed: Pentagon",
    "paragraphs": [
    "WASHINGTON (Reuters) - Russian jets bombed a target east of the Euphrates River near Dayr Az Zawat in Syria where it knew US-backed Syrian Democratic Forces and coalition advisers were located, the Pentagon said on Saturday.",
    "\"Russian munitions impacted a location known to the Russians to contain Syrian Democratic Forces and coalition advisers,\" the Pentagon said in a statement. \"Several SDF fighters were wounded,\" it added. ",
    "Coalition troops advising and assisting the SDF were not wounded, the Pentagon added. The SDF is an alliance of Kurdish and Arab militias fighting with the U.S.-led coalition.",
    "",
    " (Reporting by Lesley Wroughton; Editing by David Gregorio)"
    ],
    "dateCreated": "2017-09-16T18:29:12Z",
    "priority": 4,
    "categories": [
    "Business",
    "Economy",
    "Politics",
    "Top News",
    "Africa",
    "Europe",
    "Middle East",
    "US"
    ],
    "topics": [
    {
    "name": "War_Conflict",
    "confidence": 1
    },
    {
    "name": "Politics",
    "confidence": 1
    }
    ],
    "socialTags": [
    {
    "name": "Governorates of Syria",
    "importance": 1
    },
    {
    "name": "Syrian Civil War",
    "importance": 1
    },
    {
    "name": "Al-Hasakah District",
    "importance": 2
    },
    {
    "name": "Manbij District",
    "importance": 2
    },
    {
    "name": "Military",
    "importance": 1
    },
    {
    "name": "Syrian Democratic Forces",
    "importance": 2
    },
    {
    "name": "Rojava",
    "importance": 2
    },
    {
    "name": "Battle of Tabqa",
    "importance": 2
    },
    {
    "name": "Raqqa campaign",
    "importance": 2
    }
    ],
    "industries": []
    },
    {
    "id": "tag:reuters.com,2017:newsml_KCN1BR0Q5:2",
    "title": "Russia knew U.S.-backed Syrian forces were in area it bombed: Pentagon",
    "paragraphs": [
    "WASHINGTON (Reuters) - Russian jets bombed a target east of the Euphrates River near Dayr Az Zawr in Syria where it knew US-backed Syrian Democratic Forces and coalition advisers were located, the Pentagon said on Saturday.",
    "\"Russian munitions impacted a location known to the Russians to contain Syrian Democratic Forces and coalition advisers,\" the Pentagon said in a statement. \"Several SDF fighters were wounded,\" it added. ",
    "Coalition troops advising and assisting the SDF were not wounded, the Pentagon added. The SDF is an alliance of Kurdish and Arab militias fighting with the U.S.-led coalition.",
    "(This story corrects spelling to Dayr Az Zawr, not Dayr Az Zawat in first paragraph)",
    "",
    " (Reporting by Lesley Wroughton; Editing by David Gregorio)"
    ],
    "dateCreated": "2017-09-16T18:52:52Z",
    "priority": 4,
    "categories": [
    "Business",
    "Economy",
    "Politics",
    "Top News",
    "Africa",
    "Europe",
    "Middle East",
    "US"
    ],
    "topics": [
    {
    "name": "War_Conflict",
    "confidence": 0.983
    },
    {
    "name": "Politics",
    "confidence": 1
    }
    ],
    "socialTags": [
    {
    "name": "Syrian Democratic Forces",
    "importance": 2
    },
    {
    "name": "Raqqa campaign",
    "importance": 2
    },
    {
    "name": "Governorates of Syria",
    "importance": 1
    },
    {
    "name": "Al-Hasakah District",
    "importance": 2
    },
    {
    "name": "Rojava",
    "importance": 2
    },
    {
    "name": "Military",
    "importance": 1
    },
    {
    "name": "Manbij District",
    "importance": 2
    },
    {
    "name": "Syrian Civil War",
    "importance": 1
    },
    {
    "name": "Battle of Tabqa",
    "importance": 2
    },
    {
    "name": "Azaz District",
    "importance": 2
    },
    {
    "name": "Deir ez-Zor",
    "importance": 2
    }
    ],
    "industries": []
    }
]
},
*/