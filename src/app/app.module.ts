import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HomeModule } from './home/home.module';
import { TopicDetailModule } from './topicDetail/topicDetail.module';
import { ArticleModule } from './article/article.module';
import { AboutModule } from './about/about.module';

import 'bootstrap/dist/css/bootstrap.css';
import '../styles/main.css';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        JsonpModule,
        AppRoutingModule,
        HomeModule,
        TopicDetailModule,
        AboutModule,
        ArticleModule
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})

export class AppModule {}
