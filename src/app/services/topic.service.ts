import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Topic } from '../topic';

/*
const TOPICS: Topic[] = [
    { id: 1, title: "North Korean Missile Crisis" },
    { id: 2, title: "Trump" },
];
*/

const api = "http://localhost:9000";

@Injectable()
export class TopicService {
    private topicPath = "/topics";
    private topics: Topic[];

    constructor(private http: Http) { 
        this.loadTopics();
    }

    getTopics(): Promise<Topic[]> {
        if (this.topics) {
            return Promise.resolve(this.topics);
        }
        else {
            return this.loadTopics();
        }
    }

    private loadTopics(): Promise<Topic[]> {
        return this.http.get(api + this.topicPath)
             .toPromise()
             .then(response => {
                 this.topics = response.json() as Topic[];
                 return this.topics;
             })
             .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}