import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

import { TopicService } from '../services/topic.service';
import { Topic } from '../topic';
import { Article } from '../topic';

/*
const TOPICS: Topic[] = [
    { id: 1, title: "asdfaf" },
    { id: 2, title: "sdfgdggggg" },
];
*/

@Component({
    selector: 'topicDetail',
    templateUrl: './topicDetail.component.html',
    styleUrls: ['./topicDetail.styles.css'],
    providers: [TopicService]
})

export class TopicDetailComponent implements OnInit {
    constructor(private route: ActivatedRoute,
                private router: Router,
                private topicService: TopicService) { }

    //topics = TOPICS;
    topic: Topic;
    topicId: number;
    articles: Article[];

    ngOnInit(): void {
        this.topicId = this.route.snapshot.params['id'];

        this.topicService.getTopics().then(topics => {
            this.topic = topics[this.topicId];
            this.articles = this.topic.articles.slice(0,7);
        });
    }

    onSelect(topicIndex: number, articleIndex: number): void {
        this.router.navigate(['./topic/' + topicIndex + '/article/' + articleIndex]);
    }
}
