import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopicDetailComponent }  from './topicDetail.component';

@NgModule({
    imports: [CommonModule],
    declarations: [TopicDetailComponent]
})

export class TopicDetailModule {}
