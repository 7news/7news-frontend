## 7-news
> This project is generated with [generator-angular-project](https://github.com/shibbir/generator-angular-project) version 0.2.4.

## Installation

```bash
$ yarn start
```

## TypeScript Linting
```bash
$ yarn tslint
```

## Production Build
```bash
$ yarn build
```

## Running Unit Tests
```bash
$ yarn test
```

## Running End-to-End Tests
```bash
# make sure you have a running app
$ yarn e2e
```

## License
<a href="https://opensource.org/licenses/Apache-2.0">Apache-2.0 License</a>
